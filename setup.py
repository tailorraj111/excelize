# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in excelize/__init__.py
from excelize import __version__ as version

setup(
	name='excelize',
	version=version,
	description='Excelize',
	author='Indictrans',
	author_email='contact@indictranstech.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
