from __future__ import unicode_literals
import frappe
from frappe import _

@frappe.whitelist()
def get_user_role(user=None):
	role_lists = frappe.get_roles(user)
	role_lists.remove('All')
	role_lists.remove('Guest')
	role_lists = ','.join(role_lists)
	return role_lists
	

@frappe.whitelist()
def get_users_project_manager(doctype, txt, searchfield, start, page_len, filters):
	if filters['role'] :
		user_lst = frappe.db.sql("select u.name,u.full_name from `tabUser` as u join `tabHas Role` as r where r.parent = u.name and Not u.name = 'Administrator' and r.role = '{0}'".format(filters['role']))
		if len(user_lst):
			return user_lst

@frappe.whitelist()
def get_permission_query_condition_for_project(user):
	if user != "Administrator" and "Operations Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Resource Manager" in frappe.get_roles():
		return 0;
	elif user == "Administrator":
		return 0;
	else:
		return "`tabProject`.owner = '{0}' or `tabProject`._assign like '%%{0}%%'".format(user);

def validate(doc, Method=None):
	set_is_group(doc)
	if doc.project_manager:
		create_todo(doc.project_manager, doc.name)
		create_todo_for_assigned(doc)
		qc_check_file_name(doc)
	else:
		frappe.msgprint("warning : Project Manager is not yet selected.")

def qc_check_file_name(doc):
	file_name = frappe.db.sql("Select file_name from `tabFile` where attached_to_name ='{0}'".format(doc.name), as_list=1)
	file_name = [item for sublist in file_name for item in sublist]
	file_name  = [file for file in file_name if file.endswith('.xlsx') or file.endswith('.xls')]
	if len(file_name) < 1:
		frappe.throw("Please attached atleast one Excel file contains name QC_checklist.")
	else:
		if 'QC_checklist' not in str(file_name[0]):
			frappe.throw("Please make sure attached file contains name QC_checklist.")

def create_todo_for_assigned(doc):
	assigned_person = frappe.db.get_value("ToDo", {'reference_name' :doc.name , 'reference_type' : 'Project', 'status': 'Open'}, 'owner') 
	if assigned_person:
		for resource in doc.project_resources:
			create_todo(resource.user, doc.name)
	else:
		frappe.throw("Please assign person to the Project.")
	
def create_todo(owner, doc_name):
	to_dos = frappe.db.get_value("ToDo", {'reference_name' :doc_name , 'reference_type' : 'Project', 'status': 'Open', 'owner': owner }, 'description') 
	if to_dos == None:
		todo_doc = frappe.new_doc("ToDo")
		todo_doc.owner = owner
		todo_doc.reference_type = "Project"
		todo_doc.reference_name = doc_name
		todo_doc.assigned_by = frappe.session.user
		todo_doc.description = "Assignment for Project "+doc_name
		todo_doc.save(ignore_permissions = True)

def set_is_group(doc):
	if doc.project_template:
		task_doc_names = frappe.db.get_all("Task",{"project":doc.name})
		for task in task_doc_names:
			task_doc = frappe.get_doc("Task", task.name)
			if task_doc:
				if task_doc.parent_task == None and task_doc.is_group == 0:
					frappe.db.set_value("Task",{"name":task_doc.name},"is_milestone",1)
					frappe.db.set_value("Task",{"name":task_doc.name},"is_group",1)