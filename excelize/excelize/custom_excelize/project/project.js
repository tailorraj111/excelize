frappe.ui.form.on("Project", {
	refresh: function(frm) {
		if (frm.doc.__islocal){
			frm.toggle_display("project_manager",0)
		}
		else{
			frm.toggle_display("project_manager",1)	
		}
		if (frappe.user.has_role("Operations Manager")){
			frm.set_df_property("project_manager","read_only",0)
			frm.set_df_property("approved_allocated_hours","read_only",0)
		}
		else{
			frm.set_df_property("project_manager","read_only",1)
			frm.set_df_property("approved_allocated_hours","read_only",1)
		}
		if(frappe.user.has_role(["Projects Manager","Operations Manager","Resource Manager"])){
			frm.toggle_display('project_details',1)
		}
		else{
			frm.toggle_display('project_details',0)
		}
	}


	
});
cur_frm.fields_dict['project_manager'].get_query = function(doc) {
	return {
		query:"excelize.excelize.custom_excelize.project.project.get_users_project_manager",
		filters: {
					"role" : "Projects Manager",
		},
	}
};

frappe.ui.form.on("Project Resources", "user", function(frm, cdt, cdn) {

	var child = locals[cdt][cdn];
	frappe.call({
		method: "excelize.excelize.custom_excelize.project.project.get_user_role",
		args: {
			user:child.user ,
		},
		callback: function(r) {
			if(r.message) {
				frappe.model.set_value(cdt, cdn, "role",r.message);
			}
		}
	});
});
