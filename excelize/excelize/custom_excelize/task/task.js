frappe.ui.form.on("Task", {
	onload: function(frm) {
		if (frappe.user.has_role("Projects Manager")){
			frm.set_df_property("qc_checklist_required","read_only",0)
		}
		else{
			frm.set_df_property("qc_checklist_required","read_only",1)
		}
		project_manager = frappe.model.get_value("Project",{"name":frm.doc.project},"project_manager")
	},
	refresh: function(frm) {
		if (frappe.user.has_role("Projects User")){
            set_field_options("status", ["Open", "Working", "Submitted for Review"])
        }
        else if (frm.doc.type in ['Downtime', 'Training'] && frappe.user.has_role("Projects User")){
			set_field_options("status", ["Open", "Working", "Submitted for Review","Completed"])
		}
	
		else if (frappe.user.has_role("Project Lead")){
            set_field_options("status", ["Open", "Working", "Submitted for Review", "Submitted for PM Review"])
		}	 
		else if (frm.doc.type in ['Downtime', 'Training'] && frappe.user.has_role("Project Lead") ){
			set_field_options("status", ["Open", "Working", "Submitted for Review","Submitted for PM Review","Completed"])
			
		}
	}
});