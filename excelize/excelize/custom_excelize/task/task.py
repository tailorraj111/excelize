from __future__ import unicode_literals
import frappe
from frappe import _
from excelize.excelize.custom_excelize.project.project import qc_check_file_name
from frappe.utils import flt

@frappe.whitelist()
def get_permission_query_condition_for_task(user):
	if user != "Administrator" and "Operations Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Projects Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Resource Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Project Lead" in frappe.get_roles():
		return 0;
	elif user == "Administrator":
		return 0;
	else:
		return "`tabTask`.owner = '{0}' or `tabTask`._assign like '%%{0}%%'".format(user);

def validate(doc, Method=None):
	if doc.expected_time:
		get_expected_time(doc)
	if ((doc.qc_checklist_required == 1) and (doc.status == "Submitted for Review")):
		qc_check_file_name(doc)

def get_expected_time(doc):
	total_expected_time = frappe.db.sql("""select sum(expected_time) -(select expected_time from `tabTask` where name='{0}') from `tabTask` where project = '{1}';
""".format(doc.name, doc.project))
	approved_allocated_hrs = frappe.db.get_value("Project", { 'name' : doc.project }, 'approved_allocated_hours')
	if approved_allocated_hrs < (flt(total_expected_time[0][0]) + flt(doc.expected_time)) :
		frappe.throw("You are extending hours than the Approved allocated hours for your project")